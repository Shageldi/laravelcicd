FROM php:7.4-fpm

RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update && apt-get install -y supervisor

RUN mkdir -p "/etc/supervisor/logs"

CMD supervisorctl update

COPY supervisor/conf.d/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

CMD ["/usr/bin/supervisord", "-n", "-c",  "/etc/supervisor/conf.d/supervisord.conf"]

EXPOSE 6001